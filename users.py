import mysql.connector
import hashlib
import menu

def connect():
    mydb = mysql.connector.connect(
        host = "localhost",
        user = "root",
        password = "root",
        database="baza",
        port="3306"
    )
    return mydb

def login():
    email = input("Podaj adres e-mail: ")
    password = input("Podaj hasło: ")

    hashed_password = hashlib.sha256(password.encode("UTF-8")).hexdigest()

    user = get_user_by_email(email)

    if hashed_password == user[2]:
        print("Zalogowano")
        exit(0)
    else:
        print("Niepoprawne hasło lub email.")
        menu.show_menu()

def get_user_by_email(email):
    mydb = connect()

    sql = f"SELECT * FROM users WHERE email LIKE '{email}'"
    mycursor = mydb.cursor()
    mycursor.execute(sql)
    return mycursor.fetchone()

def users_list():
    mydb = connect()

    sql = "SELECT * FROM users"
    mycursor = mydb.cursor()
    mycursor.execute(sql)
    users =  mycursor.fetchall()

    for user in users:
        print(f"Id: {user[0]}")
        print(f"Email: {user[1]}")
        print(f"Password: {user[2]}")
        print(f"Age: {user[3]}")
        print("--------------------")
    menu.show_menu()

def register():
    email = input("Podaj adres email: ")
    password = get_password()    
    age = get_age()
    is_not_email_in_db = validate_email(email)

    hashed_password = hashlib.sha256(password.encode("UTF-8")).hexdigest()
    if not is_not_email_in_db:
        print("Konto z podanym adresem email już istnieje")
        menu.show_menu()
    register_account(email, hashed_password, age)

def validate_email(email):
    mydb = connect()

    sql = f"SELECT * FROM users WHERE email LIKE '{email}'"

    mycursor = mydb.cursor()
    mycursor.execute(sql)
    users = mycursor.fetchall()

    if len(users) == 0:
        return True
    else:
        return False

def register_account(email, password, age):
    mydb = connect()

    mycursor = mydb.cursor()

    sql = f"INSERT INTO users (email, password, age) VALUES ('{email}', '{password}', '{age}')"
    mycursor.execute(sql)
    mydb.commit()
    print(f"Zarejestrowano poprawnie użytkownika {email}")
    menu.show_menu()


def get_password():
    while True:
        password = input("Podaj hasło: ")
        password2 = input("Powtórz hasło: ")
        if password == password2:
            return password
        else:
            print("Podane hasła nie są zgodne.")

def get_age():
    while True:
        try:
            return int(input("Podaj wiek: "))
        except:
            print("Podano nieprawidłowy wiek. Spróbuj ponownie.")
