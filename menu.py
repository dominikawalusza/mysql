# Użytkownik = email + hasło + wiek

import users

def show_menu():
    print("""========MENU========
    1. Zarejestruj użytkownika
    2. Lista użytkowników
    3. Logowanie
    4. Zakończ
    """)

    choice = get_choice()
    if choice == 1:
        users.register()
    elif choice == 2:
        users.users_list()
    elif choice == 3:
        users.login()
    elif choice == 4:
        exit(0)

def get_choice():
    try:
        return int(input())
    except:
        print("Podano nieprawidłową wartość. Spróbuj ponownie.")
        show_menu()